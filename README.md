# bbc-get-querier

This project is a program that takes a new-line separated list of web addresses, and performs a GET request on each of them. 
It then outputs a stream of JSON documents containing information about the response, and finally outputs some overall data.

REQUIREMENTS:

For running the unit tests and compiling the jar file, please make sure that a server for accepting requests is running on port 8080 (127.0.0.1:8080),
otherwise the tests (and therefore also the build) will fail.

RUNNING THE PROGRAM:

Please compile the jar with maven using 'mvn package' in the bbc-get-querier directory,
 or run directory from the bin folder attached (a precompiled version) using:

java -jar bbc-get-querier-0.0.1-SNAPSHOT.jar

The program will ask for a path to a file - please put a path to a simple text file containing URLs to request, separated by 
a newline character. An example can be found in the test/ folder. 

It will process the file and output the JSONs to STDOUT (By default the terminal, 
but can be redirected using > on Linux/UNIX machines).

When you are finished inputting file paths, please type FINISH to exit the program.
