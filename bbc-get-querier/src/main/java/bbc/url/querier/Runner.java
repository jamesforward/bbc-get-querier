/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bbc.url.querier;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author james
 */
@Component
public class Runner implements CommandLineRunner
{
    @Override
    public void run(String... args) throws Exception
    {
        Scanner stringScanner = new Scanner(System.in);
        
        String read = null;
        // Whilst this isn't actually an error, we do not want 
        //this to appear in the output file.
        System.err.println("Please enter the filename of the file you wish to parse, or type 'FINISH' to exit.");
        while(!(read = stringScanner.nextLine()).equals("FINISH"))
        {
            Path filePath = Paths.get(read);
            if(!filePath.toFile().exists() || filePath.toFile().isDirectory())
            {
                System.err.println("The path input was not valid. Please try again, or type 'FINISH' to exit.");
                continue;
            }
            try(BufferedReader reader = Files.newBufferedReader(Paths.get(read)))
            {
                Map<String, JSONObject> processedJsons = reader
                        //Using lines API
                        .lines()
                        //Filters only valid URLs
                        .filter(UrlQuerier::isValidUrl)
                        //Collects to a Map<String,JSONObject> where the key is the URL)
                        .collect(Collectors.toMap(e -> e, e -> UrlQuerier.retrieveJson(e), (e,e1) -> e));
                
                // Filters out non-responsive requests, such as timeouts. 
                
                processedJsons = processedJsons.entrySet()
                        .stream()
                        .filter(e -> e.getValue().length() > 0)
                        .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
                
                System.out.println("JSON Response Documents:");
                System.out.println("");
                processedJsons.forEach((url,json) ->
                {
                    try
                    {
                        System.out.println(json.toString(4));
                    }
                    catch (JSONException ex)
                    {
                        System.err.println(ex.toString());
                    }
                });
                
                System.out.println("");
                Map<Integer,Integer> statusCodeCount = new HashMap<>();
                processedJsons.forEach((url,json) ->
                {
                    try
                    {
                        int statusCode = json.getInt("Status_code");
                        if(!statusCodeCount.containsKey(statusCode))
                        {
                            statusCodeCount.put(statusCode, 1);
                        }
                        else
                        {
                            statusCodeCount.put(statusCode, statusCodeCount.get(statusCode)+1);
                        }
                    }
                    catch (JSONException ex)
                    {
                        System.err.println(ex.toString());
                    }
                });
                
                JSONArray responseArray = new JSONArray();
                statusCodeCount.forEach((statusCode, numberOfResponses) -> {
                    JSONObject object = new JSONObject();
                    try
                    {
                        object.put("Status_code", statusCode);
                        object.put("Number_of_responses", numberOfResponses);
                        responseArray.put(object);
                    }
                    catch (JSONException ex)
                    {
                        System.err.println(ex.toString());
                    }
                });
                System.out.println("Status Code Response Documents:");
                System.out.println(responseArray.toString(4));
            }
            finally
            {
                System.err.println("Please enter the filename of the file you wish to parse, or type 'FINISH' to exit.");
            }
        }
    }
    
}
