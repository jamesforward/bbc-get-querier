/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bbc.url.querier;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.Header;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

/**
 *
 * @author james
 */
public class UrlQuerier
{

    /**
     * For this current version, not implementing username/password support.
     *
     * @param urlToRetrieve
     * @return
     */
    public static JSONObject retrieveJson(String urlToRetrieve)
    {
        int timeout = 10;
        RequestConfig globalConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.DEFAULT)
                .setConnectTimeout(timeout * 1000) // Timeout of 10 seconds
                .setConnectionRequestTimeout(timeout * 1000)
                .setSocketTimeout(timeout * 1000).build();
        RequestConfig localConfig = RequestConfig.copy(globalConfig)
                .setCookieSpec(CookieSpecs.STANDARD)
                .build();
        HttpGet getRequest = new HttpGet(urlToRetrieve);
        getRequest.setConfig(localConfig); 
        // Creating a HttpClient and Response using try-with-resources for automatic closure
        try (
             CloseableHttpClient client = HttpClients.custom()
                .setDefaultRequestConfig(globalConfig)
                .build();
             CloseableHttpResponse response = client.execute(getRequest)
            )
        {
            //Important attributes for building.
            int statusCode = response.getStatusLine().getStatusCode();
            Map<String, String> headers = 
                    Arrays.asList(response.getAllHeaders())
                    .stream()
                    .collect(Collectors.toMap(h -> h.getName(), h -> h.getValue(), (e,e1) -> e));
            JSONObject objectToReturn = new JSONObject();
            objectToReturn.put("Url", urlToRetrieve);
            objectToReturn.put("Status_code", statusCode);
            String contentLength = headers.get("Content-Length");
            if(contentLength != null)
            {
                objectToReturn.put("Content_length", Integer.parseInt(contentLength));
            }
            else if(headers.get("Transfer-Encoding").equals("chunked"))
            {
                objectToReturn.put("Content_length", "chunked");
            }
            String date = headers.get("Date");
            objectToReturn.put("Date", date);
            
            return objectToReturn;
        }
        catch (IOException | JSONException e)
        {
            System.err.println(e.toString());
        }
        
        return new JSONObject();
    }
    
    /**
     * Takes a list of Strings and returns a Map<String,JSONObject> of the results
     * of the retrieveJson method above.
     * @param toString
     * @return 
     */
    public static Map<String,JSONObject> batchRetrieveJson(String toString)
    {
        List<String> stringOfUrls = Arrays.asList(toString.split(System.lineSeparator()));
        Map<String, JSONObject> jsonMap = //The resulting map we want
                stringOfUrls // Take the list of strings
                .stream() // Use the Java Stream API
                .collect(Collectors.toMap // Use the mapping method
                (
                        e -> e, // The key will be the url in the list
                        UrlQuerier::retrieveJson, // The value will be the value of the result of retrieveJson
                        (e,e2) -> e) // Merge any duplicates
                );
        return jsonMap;
    }
    
    public static boolean isValidUrl(String url)
    {
        // Checks basic structure.
        if(!url.startsWith("http://") && !url.startsWith("https://"))
        {
            printInvalidUrl(url);
            return false;
        }
        
        UrlValidator urlValidator = new UrlValidator(UrlValidator.ALLOW_LOCAL_URLS);
        boolean valid = urlValidator.isValid(url);
        if(!valid)
        {
            printInvalidUrl(url);
        }
        return valid;
    }
    
    public static void printInvalidUrl(String url)
    {
        System.err.println("The URL at: "+url+" is not valid, and will not be processed.");
    }
}
