/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bbc.url.querier;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import org.json.JSONException;
import org.json.JSONObject;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author james
 */
public class UrlQuerierNGTest
{

    /**
     * Testing the retrieval of a single Json. Tests whatever is running on
     * local 8080 port.
     *
     * @throws org.json.JSONException
     */
    @Test
    public void testRetrieveJson() throws JSONException
    {
        String urlToGet = "http://127.0.0.1:8080";
        JSONObject retrieveJson = UrlQuerier.retrieveJson(urlToGet);
        verifyJson(retrieveJson, urlToGet);
    }

    public void verifyJson(JSONObject retrieveJson, String jsonUrl) throws JSONException
    {
        assertNotNull(retrieveJson); // Must at least return some response
        int contentLength = retrieveJson.getInt("Content_length");
        assertNotEquals(contentLength, 0); // No explicit assertEquals here as this will usually change
        int statusCode = retrieveJson.getInt("Status_code");
        assertEquals(statusCode, 200); // This should always be 200, unless the url is down or internet connectivity is bad
        String jsonUrlAttribute = retrieveJson.getString("Url");
        assertEquals(jsonUrl, jsonUrlAttribute); // These should be equal

    }

    @Test
    public void testUrlValidation()
    {
        assertTrue(UrlQuerier.isValidUrl("https://www.bbc.co.uk/iplayer"));
        assertTrue(UrlQuerier.isValidUrl("https://localhost/"));
        assertFalse(UrlQuerier.isValidUrl("bad://address"));
    }
}
